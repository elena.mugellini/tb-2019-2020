---
version: 1
titre: Cyber-Therapy, Phobia Treatment through Adaptive VR
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
mandants:
  - Chiara Lucifora (University of Messina, Italy)
professeurs co-superviseurs:
  - Elena Mugellini
  - Leonardo Angelini
instituts: [HumanTech]
mots-clé: [VR, physiological signals, unity]
langue: [F,E]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth,height=\textheight]{img/phobiaVR.jpg}
\end{center}
```

##         Description

Virtual Reality (VR) allows its users to feel immersed in a temporally and spatially different place. This peculiarity makes VR particularly interesting in the psychiartric field and in particular for the treatment of anxiety disorders.
Cyber-Therapy is a new research project based on the relationship between Computer Science and Psychology carried on by the HumanTech Institute and the University of Messina.
Our goal is to build a Virtual Reality Environment for the treatment of different phobias, such as arachnophobia, glossophobia and post traumatic stress disorder (PTSD). This environment should adapt in real-time according to the the degree of anxiety and fear of the user.
To this purpose, the software should process in real-time the user's biofeedback signals (heart rate, body temperature, state of tension, etc.) and the estimated degree of fear should be used to adapt the phobia stimuli in the VR environment.

## Objectifs/Tâches

- Elaboration of the scope statement
- Analysis of existing frameworks and projects for biofeedback signals real-time elaboration for understanding the user's stauts
- Analysis of existing phobia-related VR environments
- Implementation of a system architecture for adapting VR scenes in real-time based on user's status
- Implementation of a VR scenario of a phobia treatment with VR scenes adapted in real-time based on user's status
- Test and validation of the system with a few users

## Contraintes

- The VR environment will be developed in Unity and Oculus Rift
