---
version: 1
titre: NLU on the edge
filières:
  - Informatique
nombre d'étudiants: 1
mandants:
  - Deeplink SA
professeurs co-superviseurs:
  - Jacky Casas
  - Karl Daher
  - Elena Mugellini
attribué à: [Nicolas Crausaz]
instituts: [HumanTech]
mots-clé: [Transformer, Natural language processing, Natural language understanding, Chatbot]
langue: [F, E]
confidentialité: non
suite: non
---

## Description

Transformers model like bert are more and more widely used. Recent research around simplifying them has led to models like [Huggingface DistilBert](https://medium.com/huggingface/distilbert-8cf3380435b5) or [Google Mobile Bert](https://www.tensorflow.org/lite/models/bert_qa/overview).
 
Currently, all NLU solutions run on dedicated servers (RASA on self hosted platform, Dialogflow, Luis and Co as SaaS). The goal of this project is to evaluate and prototype the possibility to run a modern "light" transformer NLU (like Distilbert) on edge. It could be on a browser using Javascript and [tensorflow.js](https://www.tensorflow.org/js) or on Android / iPhone using [Tensorflow Lite](https://www.tensorflow.org/lite) or [pyTorch mobile](https://pytorch.org/mobile/home/).
 
By running, only the inference needs to be on edge. The training of the network should remain on a computer with access to a GPU.
 
What is expected at the end of the project is a report evaluating the possibilities and the challenges of running such a model on edge devices and if possible a prototype. In the best case scenario, we would like that for Javascript in the browser and for Android or iPhone, but the priority is for Javascript since more and more bots will be developed for the browsers.


## Tasks

- Specifications of the project
- Analysis of the existing systems and tools, choice of the most appropriate solution
- Design of the system
- Implementation
- Evaluation of the prototype
- Writing of adequate documentation

```{=tex}
\begin{center}
\includegraphics[width=0.45\textwidth,height=\textheight]{img/ht.png}
\end{center}
```

```{=tex}
\begin{center}
\includegraphics[width=0.4\textwidth,height=\textheight]{img/deeplink.jpeg}
\end{center}
```

