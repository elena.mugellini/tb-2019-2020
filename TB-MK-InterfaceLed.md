---
version: 1
titre: A web-based interface to generate LED Patterns for conversational agent based on user’s emotions
filières:
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Mira El Kamali
  - Leonardo Angelini
attribué à: [Samuel Baula]
mots-clé: [Matrix voice, tangible coach, agent conversationnel, emotions]
langue: [F]
confidentialité: non
instituts: [HumanTech]
suite: oui
---

## Contexte

Afin de suivre le projet du semestre 5 pour créer un rendu visuel de l'émotion sur le coach tangible, la création d'une interface de création de motif est une idée intéressante qui permet de poursuivre le projet précédent.
Le projet PS5 était axé sur la découverte de la MATRIX voice et sur la façon dont une personne peut percevoir différentes émotions à travers des motifs et des couleurs LED. Nous avons étudié 6 émotions de base (tristesse, bonheur, excitation, peur, dégoût, colère) en donnant un questionnaire pour choisir parmi différents gifs dont les motifs et les couleurs aident à voir une émotion spécifique.

En effet, le projet précédent n'était qu'un début et un moyen de combiner un visuel à percevoir.  Gérer un pattern qui peut soutenir deux émotions ou plus devient plus facile si c’est définit par une interface interactive Par conséquent, on propose une interface qui permet de créer des patterns animés sur un cercle. Il faut donc pouvoir gérer différentes variables comme la fréquence, luminosité, l’animation etc…
En plus, l’effet de l’audio pour aussi décrire une certaine émotion est aussi importante à prendre en considération. Par consequent, cette interface doit être finalement integré avec le coach tangible et le backend service.



## Objectifs

-	Définir les variables pour créer un pattern
-	Visualiser une simulation de l’aperçu réel tenant compte de la diffusion de la lumière
-	Intégrer un audio pour utiliser durant l’affiche d’une émotion
-	Générer un script python qui contient le code nécessaire
-	Générer un GIF
-   Integrer l'interface avec le coach tangible et la partie backend.


## Contraintes

- Python
- RapberryPi 
- matrix voice
- nodejs