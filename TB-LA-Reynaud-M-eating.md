---
version: 1
titre: M-EATING, Mobile app for promoting intercultural exchanges about food traditions
filières:
  - Télécommunications
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Leonardo Angelini
attribué à: [Mickaël Reynaud]
mandants: 
  - Alessandra Rinaldi (University of Florence)
mots-clé: [Mobile app, Flutter, social integration]
langue: [F,E]
confidentialité: non
instituts: [HumanTech]
suite: oui
---

## Contexte

M-EATING is a product-service system aimed at facilitating conviviality, dialogue and communication between cultures, through sharing the traditions of food.
The system consists of an APP connected to smart urban furnishings, located in public spaces. This allows you to organize an event in which sharing the culinary traditions of your country or to participate at an event organized by others. The APP also allows you to book the furniture available in the park, designed for conviviality and communication between several people. In this way, all those interested in learning about the culinary traditions of other countries or in discovering different flavors, can meet in the same place. People have the opportunity to learn about different traditions and cultures, as well as to share and communicate experiences.
The events can become an opportunity to share further topics, which could be of interest to the participants, regarding other traditions, art etc. The members of the community can also discover, show and enhance their skills, sharing them with all.

The goal of this bachelor project is continuing the developing of the existing mobile app (developed during the PS6) and integrating a backend server for managing the proposed service. This project will be carried out in collaboration with designers from the University of Florence, who created an appealing app design through Adobe XD and are in contact with the stakeholders that will be interested in the application.



## Objectifs

- Improvement of the app, integrating the missing features 
- Analysis, design and development of the backend service
- Test of the app and service with potential users

## Contraintes

- Flutter
- Android
