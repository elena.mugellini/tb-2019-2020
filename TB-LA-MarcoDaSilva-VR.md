---
version: 1
titre: Immersive Virtual Reality for training farmers in pro environmental behavior
filières:
  - Télécommunications
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Leonardo Angelini
attribué à: [Kevin Da Silva Marco]
mandants: 
  - Martin Dobricki (EHB)
mots-clé: [VR, jeux sérieux, écologie, agriculture]
langue: [F,E]
confidentialité: non
suite: oui
instituts: [HumanTech]
---

## Contexte

Sensibiliser à des comportements écologiques est particulièrement importants au sein des écoles professionnelles pour les agriculteurs. 
L'institut fédéral des hautes études en formation professionnelle (IFFP) est intéressé à l'utilisation de la réalité virtuelle et des jeux sérieux pour apprendre aux étudiants des pratiques écologiques. 
Ce projet de bachelor vise à améliorer ultérieurement le jeu développé au sein des projets de semestre 5 et 6 pour le rendre plus réaliste et riche en terme de refléxions sur l'écologie.


## Objectifs

- Améliorations du réalisme de l'application
- Intégration de nouvelles boucles de jeux
- Tester l'application avec le public cible

## Contraintes

- Unity
- Oculus Rift
