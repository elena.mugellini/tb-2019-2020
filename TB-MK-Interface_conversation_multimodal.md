---
version: 1
titre: Interface de gestion de conversations multimodal
filières:
  - Informatique
nombre d'étudiants: 1
mandants:
  - Mira El Kamali
professeurs co-superviseurs:
  - Mira El Kamali
  - Leonardo Angelini
  - Elena Mugellini
mots-clé: [Dialogue Management, rasa, conversational agent, multimodal]
langue: [F, E]
confidentialité: non
instituts: [HumanTech]
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.6\textwidth,height=\textheight]{img/TB-MK-interface-conversation.png}
\end{center}
```

## Description

Un système de dialogue, ou agent conversationnel, est un système informatique destiné à converser avec un humain. Les systèmes de dialogue utilisent un ou plusieurs des modes suivants : texte, parole, graphiques, haptique, gestes et autres modes de communication, tant sur le canal d'entrée que sur le canal de sortie.

Nestore est un  agent conversationnel multimodal qui vient en differents formes d'interfaces tel que: un chatbot et un coach tangible.
Le chatbot est une application logicielle( integrée dans une application mobile Android) utilisée pour mener une conversation en ligne par texte. Le chatbot peut recevoir et envoyer du texte, des chiffres, des images et des emoticons.
Le coach tangible est un assistant vocal qui utulise le text-to-speech pour envoyer la parole à l'utulisateur. 
Les deux agents conversationnel peuvent communiquer en quatres langues (Anglais, Italien, Espagnol et Néerlandais). 

La figure 1 presente l'architecture du systeme. La logique de l'agent conversationnel Nestore est composée de deux éléments principaux, RASA Core et NLU pour la reconnaissance d'intention et la compréhension du langage naturel (NLU), un serveur Node.js pour gérer la logique de la conversation.
Comme l'agent parle quatre langues, nous avons développé une interface afin de traduire toutes les phrases. Cette dernière est également intégrée dans notre architecture et toutes les phrases sont stockées dans notre base de données.
En plus, toutes les interventions d'accompagnement sont fournies par le système d'aide à la décision(DSS), qui adaptera l'intervention en fonction des données personnelles collectées par le système ou remplies par l'utilisateur. Un module externe effectuera l'analyse sémantique de certains messages de l'utilisateur.
Nous avons ses informations sous formes des APIs. 
Comme indiqué précédemment, le chatbot et le coach tangible sont disponibles pour permettre des conversations omniprésentes avec l'entraîneur. 

L'objet de ce projet est de créer une interface qui gérera le flux des conversations selon les fonctionnalités de RASA. Cette interface doit prendre en considération l'intention détectée, le contexte, l'entité détectée, le type d'entrée et de sortie également (qu'il s'agisse d'une image, d'un numéro, d'un texte). Cette interface doit être intégrée dans le nuage Nestore . Cette interface doit également pouvoir prendre des API externes afin d'obtenir des données spécifiques. 
Cette interface devrait également être en mesure de différencier les scénarios de chatbot des scénarios de coachs tangibles et enfin cette interface devrait pouvoir construire des scénarios qui peuvent être utulisés sous forme multimodal à partir des deux interfaces. En d'autres termes, cette interface devrait avoir la possibilité d'attribuer chaque tour soit au tangible, soit au chatbot sous le même scénario.

## Contraintes 

- Le système doit permettre de choisir le type du texte qu'on a besoin d'envoyer.
- Le système doit être capable de differencier les differents scenarios entre chatbot, coach tangible
- Le systeme doit être capable de créer des conversations multimodal
- Le système doit être lié au RASA NLU et node engine pour gérer les conversations.
- L'interface doit etre capable d'integrer des external APIs pour adapter une conversation personalisé
- L'interface utilisateur doit respecter le context, le court-terme histoire des conversations.
- Le language est nodeJS afin de continuer le projet deja integré


## Objectifs/Tâches

- Elaboration d'un cahier des charges
- Analyse détaillée de la structure de l'interface par rapport à RASA
- Concevoir l'architecture du système 
- Possibilité de faire des conversations pour le chatbot, pour le coach tangible et attribué dans la même conversation un tour soit au chatbot, soit au coach tangible
- Gérer les conversations qui ont besoin des APIs externes
- Integration avec le node js server et Rasa et les deux interfaces(chatbot/coach tangible)
- Tests et validation du système
- Rédaction d'une documentation adéquate
