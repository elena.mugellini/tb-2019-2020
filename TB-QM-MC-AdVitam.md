---
version: 1
titre: Implémentation d’un modèle adaptatif pour un simulateur de conduite semi-autonome
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
mandants:
  - Marine Capallera et Quentin Meteier
professeurs co-superviseurs:
  - Elena Mugellini
  - Omar Abou Khaled 
mots-clé: [Conduite semi-autonome, Interaction Homme-Machine, Modèle adaptatif]
langue: [F]
confidentialité: non
instituts: [HumanTech]
suite: non
---

## Description

De plus en plus de voitures intègrent désormais des systèmes avancés d’assistance à la conduite. La voiture sera bientôt capable de réaliser la conduite de manière autonome et le rôle du conducteur est amené à changer dans les années à venir. En effet, le conducteur devra bientôt seulement superviser l’environnement de la voiture, afin de réagir si une situation critique intervient. Ainsi le rôle des Interactions Homme-Machine (IHM) va devenir important dans les véhicules autonomes et semi-autonomes.

Dans le cadre du projet Ad Vitam (Adaptive Driver-Vehicle InTerAction to Make future driving safer), mené par l’Institut HumanTech en collaboration avec l’Université de Fribourg et l’He-Arc, nous cherchons à optimiser la sécurité du conducteur et d’améliorer l’expérience utilisateur du conducteur présent dans la voiture.

Afin de maintenir la Situation Awareness du conducteur, différents concepts monomodaux distincts ont déjà été développés et testés: interaction haptique, visuelle et vocale. 
Le but de ce travail sera de réaliser un modèle de règle à partir de framework existants afin de sélectionner la bonne modalité à utiliser, en prenant en compte l’état du système autonome et de son environnement extérieur ainsi que l’état physiologique du conducteur.
A titre d’exemple, si le conducteur est fatigué et que la situation devient critique, le modèle préférera envoyer des vibrations dans le siège plutôt qu’une lumière sur le tableau de bord. 

Lien du projet : http://advitam.humantech.institute/ 


## Contraintes (négociables)

- Le logiciel de simulation de conduite est implémenté sous Unity (C#)
- L’état du conducteur sera classifié à partir d’un script Python
- Les différents concepts d’interaction déjà développés sont: Application mobile (Android), lumières ambiantes (Arduino), vibrations dans le siège (Unity), les priorités seront définies au cours de l’analyse.
- Utilisation d’au minimum deux modalités 
- Tous les éléments devront communiquer avec le simulateur de conduite selon le même format de données (actuellement lecture d’un JSON)
- Le langage du module contenant le modèle n’est pas restreint mais nous proposons de le faire en Python 

## Objectifs/Tâches

- Elaboration d'un cahier des charges
- Prise de connaissance des concepts existants, des langages et des outils liés au simulateur, puis choix de la solution la plus appropriée pour le système
- Choisir et adapter un modèle de règles existant 
- Implémentation du modèle adaptatif
- Tests et validation du modèle sur le simulateur de conduite (utilisation de deux modalités)
- Rédaction d'une documentation adéquate
